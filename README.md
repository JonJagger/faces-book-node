#FacesBookNode
- Look at a person's face and try to remember their name.
- Hover the cursor over their face.
- Their name will appear.
- Were you right?

After cloning this repo, assuming you're on a box running docker...
```
./faces-book-node/sh/docker_pipe.sh
```
- Your docker container will now be up on port 80
- To find your public IP address you can use
```
curl ifconfig.me
```

Implemented using
[Node](https://nodejs.org/en/),
[Express](https://expressjs.com/) and
[Docker](https://www.docker.com/)

Hi everyone!

![screenshot-2018](/img/faces-book-2018.png)
![screenshot-2017](/img/faces-book-2017.png)
